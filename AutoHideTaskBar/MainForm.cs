﻿using System;
using System.Windows.Forms;

namespace AutoHideTaskBar {
    public partial class MainForm: Form {

        public MainForm() {
            InitializeComponent();
        }

        private void SetAutoHideButton_Click(object sender, EventArgs e) {
            AutoHideOperation.SetAutoHideTaskBar();
        }

        private void UnsetAutoHideButton_Click(object sender, EventArgs e) {
            AutoHideOperation.UnsetAutoHideTaskBar();
        }

        private void ReverseAutoHideButton_Click(object sender, EventArgs e) {
            AutoHideOperation.ReverseAutoHideTaskBar();
        }
    }
}