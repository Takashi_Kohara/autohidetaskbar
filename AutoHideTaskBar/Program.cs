﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
//using System.Runtime.InteropServices;

namespace AutoHideTaskBar {
    static class Program {
        [STAThread]
        static void Main() {

            // setup console for window application
            // http://stackoverflow.com/questions/1240867/console-writeline-not-working
            // http://stackoverflow.com/questions/807998/how-do-i-create-a-c-sharp-app-that-decides-itself-whether-to-show-as-a-console-o
            //if (!AttachConsole(ATTACH_PARENT_PROCESS)) { // Attach to an parent process console
            //    AllocConsole(); // Alloc a new console
            //}

            var silent_flag = false;

            var args_array = Environment.GetCommandLineArgs();
            var args_list = new List<string>();
            args_list.AddRange(args_array);
            args_list.RemoveAt(0); // delete exe-name

            foreach (var arg in args_list) {
                var lower_arg = arg.ToLower();
                if (lower_arg.Contains("reverse")) {
                    AutoHideOperation.ReverseAutoHideTaskBar();
                }
                if (lower_arg.Contains("hide")) {
                    AutoHideOperation.SetAutoHideTaskBar();
                }
                if (lower_arg.Contains("show")) {
                    AutoHideOperation.UnsetAutoHideTaskBar();
                }
                if (lower_arg.Contains("silent")) {
                    silent_flag = true;
                }
            }

            if (!silent_flag) {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new MainForm());
            }

            //FreeConsole();
        }

        //[DllImport("kernel32.dll")]
        //private static extern bool AllocConsole();

        //[DllImport("kernel32.dll")]
        //static extern bool AttachConsole(int pid);
        //private const int ATTACH_PARENT_PROCESS = -1;

        //[DllImport("kernel32.dll", SetLastError = true)]
        //static extern int FreeConsole();
    }
}