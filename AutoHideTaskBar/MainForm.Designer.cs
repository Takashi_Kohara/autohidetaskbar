﻿namespace AutoHideTaskBar {
    partial class MainForm {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent() {
            this.SetAutoHideButton = new System.Windows.Forms.Button();
            this.UnsetAutoHideButton = new System.Windows.Forms.Button();
            this.ReverseAutoHideButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // SetAutoHideButton
            // 
            this.SetAutoHideButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SetAutoHideButton.Location = new System.Drawing.Point(50, 12);
            this.SetAutoHideButton.Name = "SetAutoHideButton";
            this.SetAutoHideButton.Size = new System.Drawing.Size(178, 65);
            this.SetAutoHideButton.TabIndex = 0;
            this.SetAutoHideButton.Text = "自動的に隠す";
            this.SetAutoHideButton.UseVisualStyleBackColor = true;
            this.SetAutoHideButton.Click += new System.EventHandler(this.SetAutoHideButton_Click);
            // 
            // UnsetAutoHideButton
            // 
            this.UnsetAutoHideButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.UnsetAutoHideButton.Location = new System.Drawing.Point(50, 90);
            this.UnsetAutoHideButton.Name = "UnsetAutoHideButton";
            this.UnsetAutoHideButton.Size = new System.Drawing.Size(178, 65);
            this.UnsetAutoHideButton.TabIndex = 1;
            this.UnsetAutoHideButton.Text = "自動的に隠さない";
            this.UnsetAutoHideButton.UseVisualStyleBackColor = true;
            this.UnsetAutoHideButton.Click += new System.EventHandler(this.UnsetAutoHideButton_Click);
            // 
            // ReverseAutoHideButton
            // 
            this.ReverseAutoHideButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReverseAutoHideButton.Location = new System.Drawing.Point(51, 167);
            this.ReverseAutoHideButton.Name = "ReverseAutoHideButton";
            this.ReverseAutoHideButton.Size = new System.Drawing.Size(177, 65);
            this.ReverseAutoHideButton.TabIndex = 2;
            this.ReverseAutoHideButton.Text = "反転";
            this.ReverseAutoHideButton.UseVisualStyleBackColor = true;
            this.ReverseAutoHideButton.Click += new System.EventHandler(this.ReverseAutoHideButton_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(278, 244);
            this.Controls.Add(this.ReverseAutoHideButton);
            this.Controls.Add(this.UnsetAutoHideButton);
            this.Controls.Add(this.SetAutoHideButton);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "タスクバー隠す設定";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button SetAutoHideButton;
        private System.Windows.Forms.Button UnsetAutoHideButton;
        private System.Windows.Forms.Button ReverseAutoHideButton;
    }
}

