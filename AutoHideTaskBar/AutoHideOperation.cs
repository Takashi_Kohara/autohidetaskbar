﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
// ReSharper disable InconsistentNaming
// ReSharper disable FieldCanBeMadeReadOnly.Local

// http://oshiete.goo.ne.jp/qa/7422940.html
// http://stackoverflow.com/questions/1381821/how-to-toggle-switch-windows-taskbar-from-show-to-auto-hide-and-vice-versa

namespace AutoHideTaskBar {
    public class AutoHideOperation {
        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        private const int SW_HIDE = 0;
        private const int SW_NORMAL = 1;

        [StructLayout(LayoutKind.Sequential)]
        struct APPBARDATA {
            // ReSharper disable MemberCanBePrivate.Local
            public int cbSize;
            public IntPtr hwnd;
            public uint uCallbackMessage;
            public uint uEdge;
            public Rectangle rc;
            public int lParam;
            // ReSharper restore MemberCanBePrivate.Local
        };

        private const int ABM_GETSTATE = 4;
        private const int ABM_SETSTATE = 10;
        private const int ABS_AUTOHIDE = 1;
        private const int ABS_ALWAYSONTOP = 2;

        [DllImport("shell32.dll")]
        static extern int SHAppBarMessage(int msg, ref APPBARDATA pbd);

        public AutoHideOperation() {
        }

        public static void SetAutoHideTaskBar() {
            // 「タスクバーを自動的に隠す」
            APPBARDATA abd = new APPBARDATA();
            abd.cbSize = Marshal.SizeOf(abd);
            abd.hwnd = FindWindow("System_TrayWnd", null);
            abd.lParam = ABS_AUTOHIDE;
            SHAppBarMessage(ABM_SETSTATE, ref abd);

            // タスクバーを非表示
            ShowWindow(FindWindow("Shell_TrayWnd", null), SW_HIDE);
        }

        public static void UnsetAutoHideTaskBar() {
            // タスクバーを常に表示
            APPBARDATA abd = new APPBARDATA();
            abd.cbSize = Marshal.SizeOf(abd);
            abd.hwnd = FindWindow("System_TrayWnd", null);
            abd.lParam = ABS_ALWAYSONTOP;
            SHAppBarMessage(ABM_SETSTATE, ref abd);

            // タスクバーを表示
            ShowWindow(FindWindow("Shell_TrayWnd", null), SW_NORMAL);
        }

        public static void ReverseAutoHideTaskBar() {
            APPBARDATA abd = new APPBARDATA();
            abd.cbSize = Marshal.SizeOf(abd);
            abd.hwnd = FindWindow("System_TrayWnd", null);
            var app_bar_state = SHAppBarMessage(ABM_GETSTATE, ref abd);

            Console.WriteLine(app_bar_state.ToString());

            if ((app_bar_state & ABS_AUTOHIDE) > 0) {
                UnsetAutoHideTaskBar();
            } else {
                SetAutoHideTaskBar();
            }
        }
    }
}